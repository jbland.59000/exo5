//definition Interface Animal

// regroupement de tous les animaux
export default interface Animal {
    _nom: string;
    _poids: number;
    _dateNaissance: Date;

    /**
     * Afficher les Infos d'un animal
     */
    display():void
}