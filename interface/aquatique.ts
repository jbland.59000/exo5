// definition Interface Aquatique
import MilieuNaturel from './milieunnaturel'

// animaux en milieu naturel aquatique
export default interface Aquatique extends MilieuNaturel{
    respirerSousLeau(): void;
    nager(): void;
}