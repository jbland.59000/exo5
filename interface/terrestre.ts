// definition Interface Terrestre
import MilieuNaturel from './milieunnaturel'
export default interface Terrestre extends MilieuNaturel {
    respirerHorsDeLeau(): void;
    marcher(): void;
}