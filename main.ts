import Chat from "./class/chat";
import Dauphin from "./class/dauphin";
import Animal from "./interface/animal";

const dauphin = new Dauphin('flipper');
const chat = new Chat('Tom');
const animaux: Animal[] = [chat, dauphin];

console.log(dauphin.age);
chat.nager();
dauphin.marcher();
chat.respirerSousLeau();
animaux.forEach(animal => {
    animal.display();
});