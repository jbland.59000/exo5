//definition class Dauphin

import Mammifere from "../abstract_class/mammifere";
import Aquatique from "../interface/aquatique";
import Terrestre from "../interface/terrestre";

// la classe Dauphin
export default class Dauphin extends Mammifere implements Aquatique, Terrestre {
    constructor( nom: string, poids?: number, dateNaissance?: Date){
        super( nom, poids || 10, dateNaissance || new Date( Date.now()));
    }
    retenirMaRespiration(): void{
        console.log("Je retiens ma respiration sour l'eau");
    }
    respirerSousLeau(): void {
        this.retenirMaRespiration();
    }
    nager():void {
        console.log("swoosh swoosh, je nage");
    }
    marcher():void {
        console.log("MUHAHAHA bientot nous allons conquerir la terre");
    }
    respirerHorsDeLeau(): void {
        console.log("Je respire hors de l'eau");
    }
    /**
     * Language du dauphin
     */
    cliquetter():void {
        console.log('clic clic clic !');
    }
    display(): void{
        console.log(`Je suis un Dauphin , mon nom est ${this._nom}, je pese ${this._poids}, et suis né le ${this.age}`);
    }
}