//definition class chat

import Mammifere from "../abstract_class/mammifere";
import Aquatique from "../interface/aquatique";
import Terrestre from "../interface/terrestre";

//La classe Chat
export default class Chat extends Mammifere implements Terrestre, Aquatique {
    constructor( nom: string, poids?: number, dateNaissance?: Date){
        super( nom, poids || 3, dateNaissance || new Date( Date.now()-2));
    }
    respirerHorsDeLeau(): void {
        console.log("Je respire hors de l'eau");
    }
    marcher():void {
        console.log("tap tap tap , je marche");
    }
    /**
     * Language du chat
     */
    miauler():void {
        console.log('Miaou !');
    }
    display(): void{
        console.log(`Je suis un Chat , mon nom est ${this._nom}, je pese ${this._poids}, et suis né le ${this.age}`);
    }
    nager():void {
        console.log("swoosh , je sais survivre 15 secondes à l'eau");
    }
    respirerSousLeau(): void {
        console.log("glou , glou , je me noie")
    }
}