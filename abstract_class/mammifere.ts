//definition abstract class Mammifere

import Animal from "../interface/animal";
//
export default abstract class Mammifere implements Animal {
    _nom: string;
    _poids: number;
    _dateNaissance: Date;

    constructor(nom: string, poids: number, dateNaissance: Date){
        this._nom = nom;
        this._poids = poids;
        this._dateNaissance = dateNaissance;
    }
    display():void {
        console.log(`Je suis un Mammifere , mon nom est ${this._nom}, je pese ${this._poids}, et suis né le ${this.age}`);
    }

    get age(): number{
        return Math.abs( new Date( Date.now() - this._dateNaissance.getTime() ).getUTCFullYear() - 1970);
    }
}